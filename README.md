I'm sick and tired of chasing this proprietary codec. 
This multi-year race is over now!

I created this script for SolusOS which does not offer the codec, but the script
is not reserved for its only use. For years with Ubuntu, I had to go and grab
it from Debian repositories...

Just run the install.sh file.
it will simply copy the required files to the right places. These files are 
healthy and come from a Manjaro installation.

They're for my personal use, I'm not trying to convince anyone that the files 
are safe. Use them in measuring the risks of this kind of practice. ;)